﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public abstract class ClusterClientBase
    {
        protected string[] ReplicaAddresses { get; set; }

        protected ClusterClientBase(string[] replicaAddresses)
        {
            ReplicaAddresses = replicaAddresses;
        }

        public abstract Task<string> ProcessRequestAsync(string query, TimeSpan timeout);
        protected abstract ILog Log { get; }

        protected static HttpWebRequest CreateRequest(string uriStr)
        {
            var request = WebRequest.CreateHttp(Uri.EscapeUriString(uriStr));
            request.Proxy = null;
            request.KeepAlive = true;
            request.ServicePoint.UseNagleAlgorithm = false;
            request.ServicePoint.ConnectionLimit = 100500;
            request.AllowAutoRedirect = false;
            return request;
        }
        
        protected async Task<string> ProcessRequestAsync(string uriStr, CancellationToken cancellationToken = default)
        {
            var request = CreateRequest(uriStr);
            return await ProcessRequestAsync(request, cancellationToken);
        }
        
        protected async Task<string> ProcessRequestAsync(HttpWebRequest request, CancellationToken cancellationToken = default)
        {
            var timer = Stopwatch.StartNew();
            
            using (var response = (HttpWebResponse) await request.GetResponseAsync())
            {
                if (response.StatusCode != HttpStatusCode.Accepted) throw new Exception();
                
                var baseUri = request.RequestUri.GetLeftPart(UriPartial.Authority);
                var location = response.Headers[HttpResponseHeader.Location];
                var result = await ProcessWaitingRequestAsync(baseUri + location, cancellationToken);
                
                
                if (!cancellationToken.IsCancellationRequested)
                {
                    Cache.AddToHistory(baseUri, timer.ElapsedMilliseconds);
                    Log.InfoFormat("Response from {0} received in {1} ms", 
                        request.RequestUri, timer.ElapsedMilliseconds);
                }
                else
                {
                    Log.InfoFormat("Request {0} was canceled after {1} ms", 
                        request.RequestUri, timer.ElapsedMilliseconds);
                }
                
                return result;
            }
        }

        private static async Task<string> ProcessWaitingRequestAsync(string uri, CancellationToken cancellationToken)
        {
            try
            {
                while (true)
                {
                    var request = CreateRequest(uri);
                    using (var response = (HttpWebResponse) await request.GetResponseAsync())
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            await Task.Delay(WaitingTime, cancellationToken);
                            cancellationToken.ThrowIfCancellationRequested();
                            continue;
                        case HttpStatusCode.SeeOther:
                            var baseUri = request.RequestUri.GetLeftPart(UriPartial.Authority);
                            var location = response.Headers[HttpResponseHeader.Location];
                            var result = await ProcessAnswerRequestAsync(baseUri + location);
                            return result;
                        default:
                            throw new Exception();
                    }
                }
            }
            catch (TaskCanceledException)
            {
                var id = uri.Split('=')[1];
                var path = new Uri(uri).GetLeftPart(UriPartial.Path);
                return await ProcessCancelRequestAsync(path + "?cancel=" + id);
            }
        }

        private static async Task<string> ProcessAnswerRequestAsync(string uri)
        {
            var request = CreateRequest(uri);

            using (var response = (HttpWebResponse) await request.GetResponseAsync())
                return await new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEndAsync();
        }

        private static async Task<string> ProcessCancelRequestAsync(string uri)
        {
            var request = CreateRequest(uri);
            using (await request.GetResponseAsync())
                return null;
        }
        
        private const int WaitingTime = 100;
    }
}
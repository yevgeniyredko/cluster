﻿using System;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class RandomClusterClient : ClusterClientBase
    {
        private readonly Random random = new Random();

        public RandomClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        public override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            using (var tokenSource = new CancellationTokenSource())
            {
                var uriCollection = Cache.FilterGray(ReplicaAddresses);
                var uri = uriCollection[random.Next(uriCollection.Length)];
            
                Log.InfoFormat("Processing {0}", uri + "?query=" + query);
            
                var resultTask = ProcessRequestAsync(uri + "?query=" + query, tokenSource.Token);
                await Task.WhenAny(resultTask, Task.Delay(timeout, tokenSource.Token));

                if (resultTask.IsCompleted && (resultTask.Exception?.InnerExceptions?.Count ?? 0) == 0)
                {
                    return resultTask.Result;
                }

                if (resultTask.IsCompleted)
                {
                    if (resultTask.Exception != null) throw resultTask.Exception;
                }
                
                tokenSource.Cancel();
                Cache.AddToGrayList(uri);
                throw new TimeoutException();
            }
        }

        protected override ILog Log { get; } = LogManager.GetLogger(typeof(RandomClusterClient));
    }
}
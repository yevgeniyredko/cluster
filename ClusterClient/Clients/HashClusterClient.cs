﻿using System;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class HashClusterClient : ClusterClientBase
    {
        private readonly Random random = new Random();

        public HashClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        public override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            var uriCollection = Cache.FilterGray(ReplicaAddresses);
            using (var tokenSource = new CancellationTokenSource())
            {
                var uri = uriCollection[ComputeHash(query) % uriCollection.Length];
            
                Log.InfoFormat("Processing {0}", uri + "?query=" + query);
            
                var resultTask = ProcessRequestAsync(uri + "?query=" + query, tokenSource.Token);
                await Task.WhenAny(resultTask, Task.Delay(timeout, tokenSource.Token));

                if (resultTask.IsCompleted && (resultTask.Exception?.InnerExceptions?.Count ?? 0) == 0)
                {
                    return resultTask.Result;
                }

                if (resultTask.IsCompleted)
                {
                    if (resultTask.Exception != null) throw resultTask.Exception;
                }
                
                tokenSource.Cancel();
                Cache.AddToGrayList(uri);
                throw new TimeoutException();
            }
        }

        protected override ILog Log { get; } = LogManager.GetLogger(typeof(RandomClusterClient));

        private static int ComputeHash(string s)
        {
            var hash = s.GetHashCode();
            return hash > 0 ? hash : -hash;
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class RoundRobinClusterClient : ClusterClientBase
    {
        public RoundRobinClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        public override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            var newTimeout = TimeSpan.FromTicks(timeout.Ticks / ReplicaAddresses.Length);
            var uriCollection = Cache.GetSorted(ReplicaAddresses);

            foreach (var uri in uriCollection)
            {
                using (var tokenSource = new CancellationTokenSource())
                {
                    Log.InfoFormat("Processing {0}", uri + "?query=" + query);

                    var resultTask = ProcessRequestAsync(uri + "?query=" + query, tokenSource.Token);
                    await Task.WhenAny(resultTask, Task.Delay(newTimeout, tokenSource.Token));

                    if (resultTask.IsCompleted && (resultTask.Exception?.InnerExceptions?.Count ?? 0) == 0)
                    {
                        return resultTask.Result;
                    }
                    
                    tokenSource.Cancel();
                }
            }

            throw new TimeoutException();
        }

        protected override ILog Log { get; } = LogManager.GetLogger(typeof(RoundRobinClusterClient));
    }
}
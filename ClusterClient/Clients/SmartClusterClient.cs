﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class SmartClusterClient : ClusterClientBase
    {
        public SmartClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        public override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            using (var tokenSource = new CancellationTokenSource())
            {
                var newTimeout = TimeSpan.FromTicks(timeout.Ticks / ReplicaAddresses.Length);
                var tasks = new List<Task<string>>(ReplicaAddresses.Length);
                var uriCollection = Cache.GetSorted(ReplicaAddresses);
                
                foreach (var uri in uriCollection)
                {
                    Log.InfoFormat("Processing {0}", uri + "?query=" + query);
                
                    tasks.Add(ProcessRequestAsync(uri + "?query=" + query, tokenSource.Token));
                    var resultTask = Task.WhenAny(tasks.Where(task =>
                        !(task.IsCompleted && (task.Exception?.InnerExceptions?.Count ?? 0) > 0)));
                    await Task.WhenAny(resultTask, Task.Delay(newTimeout, tokenSource.Token));
                    
                    if (resultTask.IsCompleted && (resultTask.Result.Exception?.InnerExceptions?.Count ?? 0) == 0)
                    {
                        tokenSource.Cancel();
                        return resultTask.Result.Result;
                    }
                }
                tokenSource.Cancel();
            }

            throw new TimeoutException();
        }

        protected override ILog Log { get; } = LogManager.GetLogger(typeof(SmartClusterClient));
    }
}
﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class WaitAnyClusterClient : ClusterClientBase
    {
        public WaitAnyClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        public override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            using (var tokenSource = new CancellationTokenSource())
            {
                var tasks = Cache.FilterGray(ReplicaAddresses).Select(uri =>
                {
                    Log.InfoFormat("Processing {0}", uri + "?query=" + query);
                    return ProcessRequestAsync(uri + uri + "?query=" + query, tokenSource.Token);
                }).ToArray();

                var resultTask = Task.Run(async () =>
                {
                    while (true)
                    {
                        var runningTasks = tasks.Where(task =>
                            !(task.IsCompleted && (task.Exception?.InnerExceptions?.Count ?? 0) > 0)).ToArray();
                    
                        if (runningTasks.Length == 0)
                            throw new Exception();
                    
                        var result = await Task.WhenAny(runningTasks);
                        if (result.IsCompleted && (result.Exception?.InnerExceptions?.Count ?? 0) == 0)
                        {
                            return result.Result;
                        }
                    }
                }, tokenSource.Token);

                await Task.WhenAny(resultTask, Task.Delay(timeout, tokenSource.Token));
                
                if (resultTask.IsCompleted && (resultTask.Exception?.InnerExceptions?.Count ?? 0) == 0)
                {
                    return resultTask.Result;
                }

                if (resultTask.IsCompleted)
                {
                    if (resultTask.Exception != null) throw resultTask.Exception;
                }
            
                tokenSource.Cancel();
            }

            throw new TimeoutException();
        }

        protected override ILog Log { get; } = LogManager.GetLogger(typeof(WaitAnyClusterClient));
    }
}
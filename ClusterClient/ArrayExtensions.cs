﻿using System;
using System.Linq;

namespace ClusterClient
{
    public static class ArrayExtensions
    {
        public static T[] GetShuffled<T> (this T[] array)
        {
            return array.OrderBy(i => Guid.NewGuid()).ToArray();
        }
    }
}
﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ClusterClient
{
    public static class Cache
    {
        public static bool IsLoaded { get; private set; } = false;
        
        public static bool TryLoadFromFile(string filename)
        {
            if (!File.Exists(filename))
                return false;
            
            foreach (var line in File.ReadLines(filename))
            {
                var lineSplit = line.Split(new[] {"@@"}, StringSplitOptions.None);

                var uri = lineSplit[0];
                
                if (lineSplit.Length == 2)
                {
                    var responseTime = int.Parse(lineSplit[1]);
                
                    History[uri] = responseTime;
                    maxResponseTime = Math.Max(responseTime, maxResponseTime);
                    minResponseTime = Math.Min(responseTime, minResponseTime);
                }
                if (lineSplit.Length == 3)
                {
                    var addedTime = DateTime.FromFileTime(long.Parse(lineSplit[2]));
                    GrayList[uri] = addedTime;
                }
            }

            IsLoaded = true;
            return true;
        }

        public static void SaveToFile(string filename)
        {
            File.WriteAllLines(filename,
                CurrentHistory
                    .Where(uri => !CurrentGrayList.ContainsKey(uri.Key))
                    .Select(k => $"{k.Key}@@{k.Value}")
                    .Concat(GrayList
                        .Where(k => IsInGrayList(k.Key))
                        .Concat(CurrentGrayList)
                        .Select(k => $"{k.Key}@@gray@@{k.Value.ToFileTime()}")));
        }

        public static void AddToHistory(string uri, long ms)
        {
            var baseUri = GetBaseUri(uri);
            CurrentHistory.AddOrUpdate(baseUri, ms, (_, old) => (old + ms) / 2);
        }

        public static long? TryReadFromHistory(string uriStr)
        {
            var baseUri = GetBaseUri(uriStr);
            if (History.TryGetValue(baseUri, out var result))
                return result;
            return null;
        }

        public static void AddToGrayList(string uri)
        {
            if (IsInGrayList(uri))
                return;
            var baseUri = GetBaseUri(uri);
            CurrentGrayList.TryAdd(baseUri, DateTime.Now);
        }

        public static void CheckGrayList(string[] replicaAdresses)
        {
            var isGrayListFull = replicaAdresses.Aggregate(true, (acc, s) => acc && GrayList.ContainsKey(s));
            if (isGrayListFull)
            {
                GrayList.Clear();
            }
        }

        public static string[] FilterGray(string[] array)
        {
            var result = array
                .Where(uri => !IsInGrayList(uri))
                .ToArray();
            if (result.Length > 0)
                return result;
            return array;
        }

        public static string[] GetSorted(string[] array)
        {
            var rnd = new Random();

            if (IsLoaded)
                return array
                    .OrderBy(key => !IsInGrayList(key)
                        ? (TryReadFromHistory(key) ?? rnd.Next(minResponseTime, maxResponseTime))
                        : long.MaxValue)
                    .ToArray();
            return array.GetShuffled();
        }

        private static readonly TimeSpan GrayLifetime = TimeSpan.FromMinutes(1);
        
        private static int maxResponseTime = 0;
        private static int minResponseTime = int.MaxValue;

        private static readonly ConcurrentDictionary<string, long> CurrentHistory =
            new ConcurrentDictionary<string, long>();

        private static readonly ConcurrentDictionary<string, DateTime> CurrentGrayList =
            new ConcurrentDictionary<string, DateTime>();

        private static readonly Dictionary<string, long> History = new Dictionary<string, long>();
        private static readonly Dictionary<string, DateTime> GrayList = new Dictionary<string, DateTime>();

        private static bool IsInGrayList(string uri)
        {
            var baseUri = GetBaseUri(uri);
            return GrayList.ContainsKey(baseUri) && GrayList[baseUri].Add(GrayLifetime) > DateTime.Now;
        }
        
        private static string GetBaseUri(string uriStr)
        {
            var uri = new Uri(uriStr);
            return uri.GetLeftPart(UriPartial.Authority);
        }
    }
}
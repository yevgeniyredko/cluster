﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace ClusterServer
{
    public class Server
    {
        public Server(int port, string methodName, int methodDuration)
        {
            this.port = port;
            this.methodName = methodName;
            this.methodDuration = methodDuration;
	        this.httpListener = new HttpListener { Prefixes = {$"http://+:{port}/{methodName}/"} };
        }

	    public void StartProcessingRequests()
	    {
		    httpListener.StartProcessingRequestsSync(CreateSyncCallback());
	    }

	    public async Task StartProcessingRequestsAsync()
	    {
		    await httpListener.StartProcessingRequestsAsync(CreateAsyncCallback());
	    }
        
	    private readonly int port;
	    private readonly string methodName;
	    private readonly int methodDuration;
	    private readonly HttpListener httpListener;
        
	    private static int requestsCount;
	    private static readonly ILog Log = LogManager.GetLogger(typeof(Server));

	    private static readonly byte[] Key = Encoding.UTF8.GetBytes("Контур.Шпора");

	    private readonly ConcurrentDictionary<int, Task<byte[]>> tasks = new ConcurrentDictionary<int, Task<byte[]>>();
	    private readonly ConcurrentDictionary<int, CancellationTokenSource> cancels = new ConcurrentDictionary<int, CancellationTokenSource>();
	    
		private Func<HttpListenerContext, Task> CreateAsyncCallback()
		{
			return async context =>
			{
				if (context.Request.QueryString.Count == 0 || context.Request.QueryString.Count > 1)
				{
					context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
					return;
				}

				var key = context.Request.QueryString.AllKeys[0];
				var queryString = context.Request.QueryString[key];
				switch (key)
				{
					case "query":
						ProcessQuery(queryString, context);
						return;
					case "queue":
						ProcessQueue(queryString, context);
						return;
					case "completed":
						await ProcessCompletedAsync(queryString, context);
						return;
					case "cancel":
						ProcessCancel(queryString, context);
						return;
					default:
						context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
						return;
				}
			};
		}
	    
	    private Action<HttpListenerContext> CreateSyncCallback()
	    {
		    var asyncCallback = CreateAsyncCallback();
		    return context => asyncCallback(context).RunSynchronously();
	    }

		private void ProcessQuery(string query, HttpListenerContext context)
		{
			var currentRequestNum = Interlocked.Increment(ref requestsCount);
			
			var source = new CancellationTokenSource();
			cancels.TryAdd(currentRequestNum, source);
			tasks.TryAdd(currentRequestNum, Task.Run(async () =>
			{
				await Task.Delay(methodDuration, source.Token);
				return source.Token.IsCancellationRequested
					? null
					: GetBase64HashBytes(query, Encoding.UTF8);
			}, source.Token));

			context.Response.StatusCode = (int) HttpStatusCode.Accepted;
			context.Response.Headers[HttpResponseHeader.Location] =
				$"/{methodName}/?queue={currentRequestNum}";
		}

		private void ProcessQueue(string query, HttpListenerContext context)
		{
			if (!int.TryParse(query, out var id))
			{
				context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
				return;
			}

			if (!tasks.TryGetValue(id, out var task))
			{
				context.Response.StatusCode = (int) HttpStatusCode.NotFound;
				return;
			}

			if (!task.IsCompleted)
			{
				context.Response.StatusCode = (int) HttpStatusCode.OK;
				return;
			}

			context.Response.StatusCode = (int) HttpStatusCode.SeeOther;
			context.Response.Headers[HttpResponseHeader.Location] =
				$"/{methodName}/?completed={id}";
		}

		private async Task ProcessCompletedAsync(string query, HttpListenerContext context)
		{
			if (!int.TryParse(query, out var id))
			{
				context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
				return;
			}

			if (!tasks.TryGetValue(id, out var task) || !task.IsCompleted)
			{
				context.Response.StatusCode = (int) HttpStatusCode.NotFound;
				return;
			}

			await context.Response.OutputStream.WriteAsync(task.Result, 0, task.Result.Length);
			tasks.TryRemove(id, out _);
			cancels.TryRemove(id, out var tokenSource);
			tokenSource.Dispose();
		}

		private void ProcessCancel(string query, HttpListenerContext context)
		{
			if (!int.TryParse(query, out var id))
			{
				context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
				return;
			}

			if (!cancels.TryRemove(id, out var tokenSource))
			{
				context.Response.StatusCode = (int) HttpStatusCode.NotFound;
				return;
			}

			using (tokenSource)
			{
				tokenSource.Cancel();
			}
			tasks.TryRemove(id, out _);
			context.Response.StatusCode = (int) HttpStatusCode.OK;
		}

		private static byte[] GetBase64HashBytes(string query, Encoding encoding)
		{
			using(var hasher = new HMACMD5(Key))
			{
				var hash = Convert.ToBase64String(hasher.ComputeHash(encoding.GetBytes(query ?? "")));
				return encoding.GetBytes(hash);
			}
		}
    }
}
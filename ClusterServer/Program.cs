﻿using System;
using log4net;
using log4net.Config;

namespace ClusterServer
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			XmlConfigurator.Configure();

			try
			{
				if (!ServerArguments.TryGetArguments(args, out var parsedArguments))
					return;

				var server = new Server(parsedArguments.Port, parsedArguments.MethodName, parsedArguments.MethodDuration);

//				Log.InfoFormat("Server is starting listening prefixes: {0}", string.Join(";", listener.Prefixes));

				if(parsedArguments.Async)
				{
					server.StartProcessingRequestsAsync().Wait();
				}
				else
				{
					server.StartProcessingRequests();
				}
			}
			catch(Exception e)
			{
				Log.Fatal(e);
			}
		}
		
		private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
	}
}
